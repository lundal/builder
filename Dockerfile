FROM docker:28.0

RUN apk add --no-cache curl git make npm yarn openjdk17 rust cargo python3 g++

ENV GRADLE_OPTS="-Dorg.gradle.daemon=false"
